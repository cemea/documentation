# Manage users

## List all available commands 

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl users
    ```

=== "Docker"
    ```bash
    docker-compose exec mobilizon mobilizon_ctl users
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.users
    ```

## Create a new user


=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl users.new <email> [<options>]
    ```

=== "Docker"
    ```bash
    docker-compose exec mobilizon mobilizon_ctl users.new <email> [<options>]
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.users.new <email> [<options>]
    ```

### Options

* `--password <password>`/ `-p <password>` - the user's password. If this option is missing, a password will be generated randomly.
* `--moderator` - make the user a moderator
* `--admin` - make the user an admin

## Show a user's details

Displays if the user has confirmed their email, if they're a moderator or an admin and their profiles.

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl users.show <email>
    ```

=== "Docker"
    ```bash
    docker-compose exec mobilizon mobilizon_ctl users.show <email>
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.users.show <email>
    ```

## Modify a user

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl users.modify <email> [<options>]
    ```

=== "Docker"
    ```bash
    docker-compose exec mobilizon mobilizon_ctl users.modify <email> [<options>]
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.users.modify <email> [<options>]
    ```

### Options

* `--email <email>` - the user's new email
* `--password <password>` - the user's new password.
* `--user` - make the user a regular user
* `--moderator` - make the user a moderator
* `--admin` - make the user an admin
* `--enable` - enable the user
* `--disable` - disable the user

## Delete a user

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl users.delete <email> [<options>]
    ```

=== "Docker"
    ```bash
    docker-compose exec mobilizon mobilizon_ctl users.delete <email> [<options>]
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.users.delete <email> [<options>]
    ```
### Options

* `--assume_yes`/`-y` Don't ask for confirmation
* `--keep_email`/`-k` Keep user entry with just email information (to prevent future registrations with same email)


## Clean unconfirmed users

*Introduced in Mobilizon 1.0.3*

Manually run the cleaning of unconfirmed users (and their profiles).

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl users.clean [<options>]
    ```

=== "Docker"
    ```bash
    docker-compose exec mobilizon mobilizon_ctl users.clean [<options>]
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.users.clean [<options>]
    ```
### Options
* `-v`, `--verbose` List the users that were deleted. Implied when using `--dry-run`.
* `-d`, `--days` The number of days after which an unconfirmed user is selected to be deleted
* `--dry-run` Run the task, but don't delete users, just list them. Implies `--verbose`.
