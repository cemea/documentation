# Source

## Pre-requisites

* A Linux machine with **root access**
* A **domain name** (or subdomain) for the Mobilizon server, e.g. `your-mobilizon-domain.com`
* An **SMTP server** to deliver emails

### Security

We advise to make sure your webserver is secure enough.
For instance you can require authentication through SSH keys and not passwords, install Fail2Ban to block repeated login attempts and block unused ports by installing and configuring a firewall.


## Dependencies

Mobilizon requires Elixir, NodeJS and PostgreSQL among other things. Prefer to install Elixir and NodeJS from their official repositories instead of your distribution's packages.

Recommended versions:

* Elixir 1.11+
* NodeJS 14+
* PostgreSQL 11+

### Upgrade system

Just to be sure your system is up-to-date before doing anything else:

=== "Ubuntu / Debian"
    ```bash
    sudo apt update
    sudo apt dist-upgrade
    ```

### Basic tools
We begin by making sure some basic tools are installed: 

=== "Ubuntu / Debian"
    ```bash
    sudo apt install build-essential curl unzip openssl git cmake file
    ```

=== "Archlinux"
    ```sh
    sudo pacman -S base-devel curl unzip openssl git cmake file
    ```

### Web server
We only officially support nginx.

=== "Ubuntu / Debian"
    ```bash
    sudo apt install nginx
    ```

### HTTPS Certificates
Then we need to install [certbot](https://certbot.eff.org/), a tool to ask for free Let's Encrypt HTTPS certificates.
 
=== "Ubuntu / Debian"
    ```bash
    sudo apt install certbot
    ```

You can use certbot with web server plugins or manually. See [Certbot's documentation](https://certbot.eff.org/instructions).


### NodeJS
We install the latest NodeJS version by adding NodeSource repos and installing NodeJS:

Head over to [this page](https://github.com/nodesource/distributions/blob/master/README.md#table-of-contents) and follow the instructions for `Node.js v12.x`.

!!! info
    Unless stated otherwise, Mobilizon always supports only the latest LTS version of NodeJS.
    
!!! tip
    NodeSource repos only gives updates for a specific version of NodeJS (it doesn't upgrade itself to a new major version). When a new major version of NodeJS is released, you need to remove the old repo and add the new one. 

### Yarn
Mobilizon uses [Yarn](https://yarnpkg.com/) to manage NodeJS packages, so we need to install it as well.

Follow the instructions on [this page](https://yarnpkg.com/en/docs/install#debian-stable) to add Yarn's repository and install it.

!!! info
    It is also possible to install `yarn` directly with `npm`: 
    ```bash
    npm install -g yarn
    ```.
    You need to make sure npm's binary packages folder in your `$PATH` afterwards to use `yarn`.

### Erlang and Elixir

The packages from Debian or Ubuntu are badly packaged and often out of date, so we need to add one final source repository.

Follow the instructions for Ubuntu / Debian on [this page](https://elixir-lang.org/install.html#unix-and-unix-like) to add Erlang Solutions repo and install Erlang and Elixir.

!!! tip
    The Erlang package also wants to install an add-on for Emacs for some reason (but it doesn't install Emacs). If you see a warning, nothing to worry about. 

### PostgreSQL and PostGIS

Mobilizon uses the [PostgreSQL](https://www.postgresql.org) database, and the PostgreSQL [Postgis](https://postgis.net) extension to store geographical data into PostgreSQL.

=== "Ubuntu / Debian"
    ```bash
    sudo apt install postgresql postgresql-contrib
    sudo apt install --install-recommends postgis
    ```


After that we can enable and start the PostgreSQL service. 
```
sudo systemctl --now enable postgresql
```

### Misc

We need the following tools to handle and optimize pictures that are uploaded on Mobilizon.

=== "Ubuntu / Debian"
    ```bash
    sudo apt install imagemagick webp
    ```

The following packages are optional, Mobilizon can run without them, but if found they will be used to optimize user's pictures so that they are lighter.

=== "Ubuntu / Debian"
    ```bash
    sudo apt install gifsicle jpegoptim optipng pngquant
    ```

Once finished, let's [head back to the install guide](index.md).

## Arch Linux

Run the following command to install all at once:
```bash
sudo pacman -S nodejs postgresql openssl git wget unzip base-devel yarn nginx elixir postgis imagemagick file libwebp
```

Now that dependencies are installed, before running Mobilizon you should start PostgreSQL:
```
sudo systemctl --now enable postgresql
```

Once finished, let's [head back to the install guide](index.md).

## Other distributions

Feel free to update this file in a merge request!


## Setup

We're going to use a dedicated `mobilizon` user with `/home/mobilizon` home:

=== "GNU/Linux"
    ```bash
    sudo adduser --shell /bin/false mobilizon
    ```

=== "FreeBSD"
    ``` bash
    sudo pw useradd -n mobilizon -d /home/mobilizon -s /usr/local/bin/bash -m 
    sudo passwd mobilizon
    ```

Then let's connect as this user:

```bash
sudo -i -u mobilizon
```

Let's start by cloning the repository in a directory named `live`:

```bash
git clone https://framagit.org/framasoft/mobilizon live && cd live
```

Then pick the latest release (you can get the number version from https://framagit.org/framasoft/mobilizon/-/releases). For instance if the version is 1.0.6:
```bash
git checkout 1.0.6
```

## Installing dependencies

Install Elixir dependencies

```bash
mix deps.get
```

!!! note
    When asked for `Shall I install Hex?` or `Shall I install rebar3?`, hit the enter key to confirm.

Then compile these dependencies and Mobilizon (this can take a few minutes, and can output all kinds of warnings, such as depreciation issues)

```bash
MIX_ENV=prod mix compile
```

Go into the `js/` directory

```bash
cd js
```

and install the Javascript dependencies

```bash
yarn install
```

Finally, we can build the front-end (this can take a few seconds).

!!! warning
    Building front-end can consume up to 2048MB of RAM by default. If it's too much or not sufficient for your setup, you can adjust the maximum memory used by prefixing the command with the following option:
    ```
    NODE_BUILD_MEMORY=1024
    ```
```bash
yarn run build
```

Let's go back to the main directory
```bash
cd ../
```

## Configuration

Mobilizon provides a command line tool to generate configuration

```bash
MIX_ENV=prod mix mobilizon.instance gen
```

This will ask you questions about your setup and your instance to generate a `runtime.exs` file in the `config/` folder, and a `setup_db.psql` file to setup the database.

### Database setup

The `setup_db.psql` file contains SQL instructions to create a PostgreSQL user and database with the chosen credentials and add the required extensions to the Mobilizon database.

Exit running as the mobilizon user (as it shouldn't have `root`/`sudo` rights) and execute in the `/home/mobilizon/live` directory:
```bash
sudo -u postgres psql -f setup_db.psql
```

It should output something like:
```
CREATE ROLE
CREATE DATABASE
You are now connected to database "mobilizon_prod" as user "postgres".
CREATE EXTENSION
CREATE EXTENSION
CREATE EXTENSION
```

Let's get back to our `mobilizon` user:
```bash
sudo -i -u mobilizon
cd live
```

!!! warning

    When it's done, don't forget to remove the `setup_db.psql` file.

### Database Migration

Run database migrations: 
```bash
MIX_ENV=prod mix ecto.migrate
```

!!! note

    Note the `MIX_ENV=prod` environment variable prefix in front of the command. You will have to use it for each `mix` command from now on.

You will have to do this again after most updates.

!!! tip
    If some migrations fail, it probably means you're not using a recent enough version of PostgreSQL, or that you haven't installed the required extensions.

### Uploads

The default uploads directory is set to `/var/lib/mobilizon/uploads`, so we need to create it. The location can be changed with the following configuration:  
```elixir
config :mobilizon, Mobilizon.Web.Upload.Uploader.Local, uploads: "/var/lib/mobilizon/uploads"
```

* Create the media directory
  ```sh
  mkdir -p /var/lib/mobilizon/uploads
  ```

* Make sure the uploaded files have the proper owner. Mobilizon needs write access to this folder.
  ```bash
  chown -R mobilizon:mobilizon /var/lib/mobilizon/uploads
  ```

## Services

We can quit using the `mobilizon` user again.

### Systemd

Copy the `support/systemd/mobilizon-source.service` to `/etc/systemd/system/mobilizon.service`.

```bash
sudo cp support/systemd/mobilizon-source.service /etc/systemd/system/mobilizon.service
```

Reload Systemd to detect your new file

```bash
sudo systemctl daemon-reload
```

And enable the service

```bash
sudo systemctl enable --now mobilizon.service
```

It will run Mobilizon and enable startup on boot. You can follow the logs with

```bash
sudo journalctl -fu mobilizon.service
```

You should see something like this:
```
Running Mobilizon.Web.Endpoint with cowboy 2.8.0 at :::4000 (http)
Access Mobilizon.Web.Endpoint at https://your-mobilizon-domain.com
```

The Mobilizon server runs on port 4000 on the local interface only, so you need to add a reverse-proxy.

## Reverse proxy

### Nginx

Copy the file from `support/nginx/mobilizon-source.conf` to `/etc/nginx/sites-available/mobilizon.conf`.

```bash
sudo cp support/nginx/mobilizon-source.conf /etc/nginx/sites-available/mobilizon.conf
```

Then symlink the file into the `/etc/nginx/sites-enabled` directory.

```bash
sudo ln -s /etc/nginx/sites-available/mobilizon.conf /etc/nginx/sites-enabled/
```

Edit the file `/etc/nginx/sites-available/mobilizon.conf` and adapt it to your own configuration.

Test the configuration with `sudo nginx -t` and reload nginx with `sudo systemctl reload nginx`.

### Let's Encrypt

The nginx configuration template handles the HTTP-01 challenge with the webroot plugin:
```bash
sudo mkdir /var/www/certbot
```

Run certbot with (don't forget to adapt the command)
```bash
sudo certbot certonly --rsa-key-size 4096 --webroot -w /var/www/certbot/ --email your@email.com --agree-tos --text --renew-hook "/usr/sbin/nginx -s reload" -d your-mobilizon-domain.com
```

Then adapt the nginx configuration `/etc/nginx/sites-available/mobilizon.conf` by uncommenting certificate paths and removing obsolete blocks.

Finish by testing the configuration with `sudo nginx -t` and reloading nginx with `sudo systemctl reload nginx`.

You should now be able to load https://your-mobilizon-domain.com inside your browser.

## Creating your first user

Login back as the `mobilizon` system user:

```bash
sudo -i -u mobilizon
cd live
```

Create a new user:
```
 MIX_ENV=prod mix mobilizon.users.new "your@email.com" --admin --password "Y0urP4ssw0rd"
```

!!! danger
    Don't forget to prefix the command with an empty space so that the chosen password isn't kept in your shell history.

!!! tip
    You can ignore the `--password` option and Mobilizon will generate one for you.

See the [full documentation](../CLI tasks/manage_users.md#create-a-new-user) for this command.

You may now login with your credentials and discover Mobilizon. Feel free to explore [configuration documentation](../configure) as well.

## Suggested tasks

### Configure emails

The default Mobilizon configuration assumes a local SMTP server is available on the same server. To tweak this for your own setup, [see this page](../configure/email.md).

### Configure 3rd-party auth

Mobilizon can use LDAP or OAuth-based login providers (Facebook, Google, etc.) to help register or login users. Configuration [can be found here](../configure/auth.md).

### Configure geocoders

This will allow the address autocomplete field to provide addresses when editing an event. The default value uses OpenStreetMap's Nominatim provider but you can [change it to the provider of your choice](../configure/geocoders.md).

!!! note
    When using the default OpenStreetMap's Nominatim provider, autocomplete is disabled and using the service implies that you accept [their Usage Policy](https://operations.osmfoundation.org/policies/nominatim/).


### Geolocation databases

Mobilizon can use geolocation from MMDB format data from sources like [MaxMind GeoIP](https://dev.maxmind.com/geoip/geoip2/geolite2/) databases or [db-ip.com](https://db-ip.com/db/download/ip-to-city-lite) databases. This allows showing events happening near the user's location.

You will need to download the City database and put it into `/var/lib/mobilizon/geo/GeoLite2-City.mmdb`. You may also customize the location with the following setting:

```elixir
config :geolix,
  databases: [
    %{
      id: :city,
      adapter: Geolix.Adapter.MMDB2,
      source: "/var/lib/mobilizon/geo/GeoLite2-City.mmdb"
    }
  ]
```

* Download the database file and rename it to `GeoLite2-City.mmdb`.

* Create the GeoIP directory
  ```sh
  mkdir -p /var/lib/mobilizon/geo
  ```

* Move the database inside the folder
  ```bash
  mv GeoLite2-City.mmdb /var/lib/mobilizon/geo/
  ```

* Make sure the GeoIP database has the proper owner. Mobilizon only needs read access to this GeoIP database file.
  ```bash
  chown -R mobilizon:mobilizon /var/lib/mobilizon/geo
  ```

Finish by restarting the `mobilizon` service.

Mobilizon will only show a warning at startup if the database is missing, but it isn't required.
