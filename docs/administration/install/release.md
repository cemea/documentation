# Release

## Pre-requisites

* A Linux machine (x86_64) with **root access**.
* A **domain name** (or subdomain) for the Mobilizon server, e.g. `your-mobilizon-domain.com`
* An **SMTP server** to deliver emails


!!! info
    We tested this guide with Debian Buster, Ubuntu 20.04 and Fedora 33.
    Installing the precompiled release on other and older distributions *may* not work due to various issues.
    It is advised to consider [building Mobilizon from source](./source.md) if you have trouble installing it on other configurations.

## Dependencies

Mobilizon only requires the PostgreSQL database engine and a few libraries:

 * *curl* (to download the release build archive)
 * *tar* (to extract the release from the archive)
 * *PostgreSQL* and *PostGIS*
 * *nginx* (other reverse proxies may work but we only support this one)
 * *certbot* (for Let's Encrypt certificates, but any ACME client could work)
 * *ImageMagick* to process uploaded pictures
 * *file* to get MIME information about files

=== "Ubuntu / Debian"
    ```
    sudo apt install curl tar postgresql postgresql-contrib nginx certbot imagemagick file
    sudo apt install --install-recommends postgis
    ```
=== "Fedora"
    ```
    sudo dnf install curl tar postgresql postgresql-contrib nginx certbot ImageMagick file
    sudo dnf install postgis postgis-upgrade
    ```

The following dependencies are optional, but will be used to optimize your users uploaded pictures if available:

=== "Ubuntu / Debian"
    ```bash
    sudo apt install gifsicle jpegoptim optipng pngquant
    ```
=== "Fedora"
    ```bash
    sudo dnf install gifsicle jpegoptim optipng pngquant
    ```

## Setup

We're going to use a dedicated `mobilizon` user:
=== "Ubuntu / Debian"
    !!! important
        We are going to install Mobilizon into `/opt/mobilizon`. You may want to install it somewhere else, like `/usr/local/lib/mobilizon`. In that case, make sure you adapt the paths in the following instructions.

    ```bash
    sudo adduser --group --system --shell /bin/false --home /opt/mobilizon mobilizon
    ```

=== "Fedora"
    !!! important
        We are going to install Mobilizon into `/opt/mobilizon`. You may want to install it somewhere else, like `/usr/local/lib/mobilizon`. In that case, make sure you adapt the paths in the following instructions.

    ```bash
    sudo adduser -D --system --shell /bin/false --home /opt/mobilizon mobilizon
    ```

=== "FreeBSD"
    !!! important
        On GNU/Linux, we install Mobilizon in `/opt/mobilizon`, but that's not an usual path on *BSD systems. We recommend `/usr/local/lib/mobilizon` instead, but you'll need to adapt the instructions with this path.
    ``` bash
    sudo pw useradd -n mobilizon -d /usr/local/lib/mobilizon -s /usr/local/bin/bash -m 
    sudo passwd mobilizon
    ```



## Download and install release

!!! info
    To get the latest package URL, you should go to [our release page](https://framagit.org/framasoft/mobilizon/-/releases) and copy the URL from the latest `tar.gz` package.
    
    The https://joinmobilizon.org/latest-package URL redirects to [the latest <ins>stable</ins> release package available](https://joinmobilizon.org/latest-release) ([Gitlab doesn't allow to do this directly currently](https://gitlab.com/gitlab-org/gitlab/-/issues/16821)), but you shouldn't use it it you want to get a beta version or a different version.

* Download the latest release build and unzip it:  
  ```sh
  curl -L 'https://joinmobilizon.org/latest-package' -o /tmp/mobilizon.tar.gz
  tar xzf /tmp/mobilizon.tar.gz -C /tmp/
  ```
* Move the release to the install folder:  
  ```sh
  mv /tmp/mobilizon /opt/
  rm /tmp/mobilizon.tar.gz
  ```

* Set the proper owner to `/opt/mobilizon`
  ```sh
  chown -R mobilizon:mobilizon /opt/mobilizon
  ```

* Create config directory
  ```sh
  mkdir -p /etc/mobilizon
  chown -R mobilizon /etc/mobilizon
  ```

* Create directories for user uploads and the GeoIP database
  ```sh
  mkdir -p /var/lib/mobilizon/uploads /var/lib/mobilizon/data
  chown -R mobilizon /var/lib/mobilizon
  ```

## Configuration

Mobilizon provides a command line tool to generate configuration

```bash
cd /opt/mobilizon
sudo -u mobilizon ./bin/mobilizon_ctl instance gen  --output /etc/mobilizon/config.exs --output-psql /tmp/setup_db.psql
```

This will ask you questions about your setup and your instance to generate a `config.exs` file in the `/etc/mobilizon/` folder, and a `/tmp/setup_db.psql` file to setup the database.

### Database setup

The `setup_db.psql` file contains SQL instructions to create a PostgreSQL user and database with the chosen credentials and add the required extensions to the Mobilizon database.

```bash
sudo -u postgres psql -f /tmp/setup_db.psql
```

It should output something like:
```
CREATE ROLE
CREATE DATABASE
You are now connected to database "mobilizon_prod" as user "postgres".
CREATE EXTENSION
CREATE EXTENSION
CREATE EXTENSION
```

If is successeds, we can remove the file:
```sh
sudo rm /tmp/setup_db.psql
```

### Database Migration

Run database migrations: 
```bash
sudo -u mobilizon ./bin/mobilizon_ctl migrate
```

You will have to do this again after most updates.

!!! tip
    If some migrations fail, it probably means you're not using a recent enough version of PostgreSQL, or that you haven't installed the required extensions.

## Services

### Systemd

Copy the `support/systemd/mobilizon-release.service` to `/etc/systemd/system/mobilizon.service`.

```bash
sudo cp support/systemd/mobilizon-release.service /etc/systemd/system/mobilizon.service
```

Reload Systemd to detect your new file

```bash
sudo systemctl daemon-reload
```

And enable the service

```bash
sudo systemctl enable --now mobilizon.service
```

It will run Mobilizon and enable startup on boot. You can follow the logs with

```bash
sudo journalctl -fu mobilizon.service
```

You should see something like this:
```
Running Mobilizon.Web.Endpoint with cowboy 2.8.0 at :::4000 (http)
Access Mobilizon.Web.Endpoint at https://your-mobilizon-domain.com
```

The Mobilizon server runs on port 4000 on the local interface only, so you need to add a reverse-proxy.

## Reverse proxy

### Nginx

Copy the file from `support/nginx/mobilizon.conf` to `/etc/nginx/sites-available/mobilizon.conf`.

```bash
sudo cp support/nginx/mobilizon-release.conf /etc/nginx/sites-available/mobilizon.conf
```

Then symlink the file into the `/etc/nginx/sites-enabled` directory.

```bash
sudo ln -s /etc/nginx/sites-available/mobilizon.conf /etc/nginx/sites-enabled/
```

Edit the file `/etc/nginx/sites-available/mobilizon.conf` and adapt it to your own configuration.

Test the configuration with `sudo nginx -t` and reload nginx with `sudo systemctl reload nginx`.

### Let's Encrypt

The nginx configuration template handles the HTTP-01 challenge with the webroot plugin:
```bash
sudo mkdir /var/www/certbot
```

Run certbot with (don't forget to adapt the command)
```bash
sudo certbot certonly --rsa-key-size 4096 --webroot -w /var/www/certbot/ --email your@email.com --agree-tos --text --renew-hook "/usr/sbin/nginx -s reload" -d your-mobilizon-domain.com
```

Then adapt the nginx configuration `/etc/nginx/sites-available/mobilizon.conf` by uncommenting certificate paths and removing obsolete blocks.

Finish by testing the configuration with `sudo nginx -t` and reloading nginx with `sudo systemctl reload nginx`.

You should now be able to load https://your-mobilizon-domain.com inside your browser.

## Creating your first user

Create a new Mobilizon user with the role admin:
```
 sudo -u mobilizon ./bin/mobilizon_ctl users.new "your@email.com" --admin --password "Y0urP4ssw0rd"
```

!!! danger
    Don't forget to prefix the command with an empty space so that the chosen password isn't kept in your shell history.

!!! tip
    You can ignore the `--password` option and Mobilizon will generate one for you.

See the [full documentation](../CLI tasks/manage_users.md#create-a-new-user) for this command.

You may now login with your credentials and discover Mobilizon. Feel free to explore [configuration documentation](../configure) as well.

## Suggested tasks

### Configure emails

The default Mobilizon configuration assumes a local SMTP server is available on the same server. To tweak this for your own setup, [see this page](../configure/email.md).

### Configure 3rd-party auth

Mobilizon can use LDAP or OAuth-based login providers (Facebook, Google, etc.) to help register or login users. Configuration [can be found here](../configure/auth.md).

### Configure geocoders

This will allow the address autocomplete field to provide addresses when editing an event. The default value uses OpenStreetMap's Nominatim provider but you can [change it to the provider of your choice](../configure/geocoders.md).

!!! note
    When using the default OpenStreetMap's Nominatim provider, autocomplete is disabled and using the service implies that you accept [their Usage Policy](https://operations.osmfoundation.org/policies/nominatim/).


### Geolocation databases

Mobilizon can use geolocation from MMDB format data from sources like [MaxMind GeoIP](https://dev.maxmind.com/geoip/geoip2/geolite2/) databases or [db-ip.com](https://db-ip.com/db/download/ip-to-city-lite) databases. This allows showing events happening near the user's location.

You will need to download the City database and put it into `priv/data/GeoLite2-City.mmdb`. Finish by restarting the `mobilizon` service.

Mobilizon will only show a warning at startup if the database is missing, but it isn't required.