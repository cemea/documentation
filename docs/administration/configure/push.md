# Push Notifications

Mobilizon can send [web Push Notifications](https://developer.mozilla.org/en-US/docs/Web/API/Push_API) through registered browsers. To allow this, you need to generate [VAPID public keys](https://datatracker.ietf.org/doc/html/rfc8292).

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl webpush.gen.keypair
    ```

=== "Docker"
    ```bash
    docker-compose exec mobilizon mobilizon_ctl webpush.gen.keypair
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.webpush.gen.keypair
    ```

The command outputs you the code to put in your configuration file. Restart Mobilizon after that, and you should be able to use browser push notifications.