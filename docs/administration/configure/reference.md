# Reference

The following is a cheat sheet for the Mobilizon configuration files.

## Instance

### :instance

* `:name` The fallback instance name if not configured into the admin UI
* `:description` The fallback instance description if not configured into the admin UI
* `:hostname` Your instance's hostname
* `:registrations_open` The fallback instance setting to open registrations if not configured into the admin UI
* `:registration_email_allowlist` A list of email domains for which the registration is allowed even if registration is closed
* `:languages` The fallback instance languages if not configured into the admin UI
* `:default_language` The instance's default language if user's language can't be found
* `:demo` Whether the instance is in demo mode
* `:allow_relay` Whether the instance can be subscribed to and can relay activites
* `:federating` Completely stop federating with the exterior
* `:upload_limit` The default maximum size for an upload
* `:avatar_upload_limit` The maximum size for an avatar upload size
* `:banner_upload_limit` The maximum size for a banner upload size
* `:remove_orphan_uploads` Whether to clean orphan uploads (unattached to entities like events or posts)
* `:orphan_upload_grace_period_hours` How long before an orphan upload can be removed
* `remove_unconfirmed_users` Whether to remove unconfirmed users (unrelated to event participation)
* `unconfirmed_user_grace_period_hours` How long before an unconfirmed user account can be removed (unrelated to event participation)
* `activity_expire_days` How long before removing group activities
* `activity_keep_number` How many activities to always keep
* `enable_instance_feeds` Activate the instance-wide feeds, which provide all of the instance public events and posts
* `email_from` The email for the `From:` header in emails
* `email_reply_to` The email for the `Reply-To:` header in emails

### :groups

* `:enabled` Whether groups are enabled

### :events

* `:enabled` Whether events are enabled

### :media_proxy

* `enabled` enable the media proxy
* `proxy_opts` options for the media proxy
    * `redirect_on_failure` whether to redirect to the original media if proxifying fails
    * `max_body_length` how big the media can be
    * `max_read_duration` how long we can wait for the remote media
    * `http`
        * `follow_redirect` whether we follow redirects

## HTTP Server

### Mobilizon.Web.Endpoint

See also the configuration reference for [Phoenix.Endpoint](https://hexdocs.pm/phoenix/Phoenix.Endpoint.html).

* `:url` configuration for generating URLs throughout the app
    * `:host` Your instance's hostname
    * `:scheme` `http` or `https`
* `:secret_key_base` A secret key used as a base to generate secrets for encrypting and signing data
* `:render_errors` defines the view that will be used to render errors
* `:pubsub_server` the name of the pubsub server to use in channels
* `:cache_static_manifest` a path to a json manifest file that contains static files and their digested version
* `:has_reverse_proxy` whether you use a reverse proxy (useful to get the user's real IP)

### :http_security

* `:enabled` Whether to add security-related HTTP headers
* `:sts` Whether to add an HSTS header
* `:sts_max_age` the max-age for the HSTS header
* `:csp_policy` a keyword-list of CSP components, made from domain lists
* `:referrer_policy` a value for the Referrer-Policy header

## Uploads

### Mobilizon.Web.Upload

* `:uploader` The module that handles the upload. See [uploaders](#uploaders)
* `:filters` A list of modules performing various tasks on uploads. See [filters](#filters)
* `:allow_list_mime_types` a list of allowed MIME types
* `:link_name` Whether to add the original file name at the end of the URL

### Uploaders

* `Mobilizon.Web.Upload.Uploader.Local` Uploads to the server in the uploads folder

#### Mobilizon.Web.Upload.Uploader.Local

* `:uploads` the folder where to put uploads in, relative to Mobilizon's working directory

### Filters

* `Mobilizon.Web.Upload.Filter.Dedupe` Depupes duplicate files by comparing their hash
* `Mobilizon.Web.Upload.Filter.Optimize` Performs multiple optimization tools on medias (see [ExOptimizer](https://hexdocs.pm/ex_optimizer))


## Email

### Mobilizon.Web.Email.Mailer

* `:adapter` One of [Bamboo's available adapters](https://github.com/thoughtbot/bamboo/#available-adapters)

See each adapter's documentation for extra parameters.

Example for [the SMTP adapter](https://github.com/fewlinesco/bamboo_smtp):
```elixir
config :mobilizon, Mobilizon.Web.Email.Mailer,
  adapter: Bamboo.SMTPAdapter,
  server: "smtp.domain",
  username: "your.name@your.domain"
  password: "pa55word",
  port: 465,
  ssl: true,
  auth: :always
```

Example for [the Sendgrid adapter](https://hexdocs.pm/bamboo/Bamboo.SendGridAdapter.html):
```elixir
config :mobilizon, Mobilizon.Web.Email.Mailer,
  adapter: Bamboo.SendGridAdapter,
  api_key: "YOUR_API_KEY"
```

## :logger

See also the configuration reference for [Logger](https://hexdocs.pm/logger/Logger.html#module-configuration).

* `:backends` A list of logger backends. See [logger backends](#logger_backends).
* `:format` Log format template
* `:metadata` What to include in the log

### Logger Backends

* `:console` Sends the logs to `stdout`.
* `Sentry.LoggerBackend` sends the logs to Sentry, which needs [to be configured](https://hexdocs.pm/sentry/7.2.4/readme.html#configuration).

## Authentication

### Mobilizon.Service.Auth.Authenticator

The module to perform the authentification, must be one of:  

  * `Mobilizon.Service.Auth.MobilizonAuthenticator` Mobilizon's default database authenticator
  * `Mobilizon.Service.Auth.LDAPAuthenticator` LDAP authentication

Authenticators always fallback on `MobilizonAuthenticator` if user is not found.

### :ldap

See the [LDAP configuration](./auth.md#ldap).

### :auth

* `:oauth_consumer_strategies` A list of auth methods to be displayed on the login page

See the [OAuth configuration](./auth.md#oauth).

## Link parsing

### Mobilizon.Service.Formatter

Configuration for Mobilizon's link formatter which parses mentions, hashtags, and URLs.

* `:class` specify the class to be added to the generated link
* `:rel` The `rel` attribute value
* `:new_window` adds target="_blank" attribute 
* `:truncate` Set to a number to truncate URLs longer then the number. Truncated URLs will end in ...
* `:strip_prefix` Strip the scheme prefix
* `:extra` link URLs with rarely used schemes (magnet, ipfs, irc, etc.)
* `:validate_tld` Set to false to disable TLD validation for URLs/emails. Can be set to :no_scheme to validate TLDs only for urls without a scheme (e.g example.com will be validated, but http://example.loki won't)

## Locales

### :cldr

Configuration for CLDR data, used to format & handle datetimes, numbers,… according to the user's locale

* `:locales` A list of CLDR-locales

## Federation

### :activitypub

* `:actor_stale_period` how long before an actor needs to be refreshed
* `:actor_key_rotation_delay` how long before an actor can rotate their keys
* `:sign_object_fetches` whether to sign object fetches

## Geospatial

### Mobilizon.Service.Geospatial

See [Geocoders](./geocoders.md).

### :maps

* `:tiles` defines the tiles server endpoint
    * `:endpoint` The templated tile endpoint URL. If you change this from the default value (openstreetmap.org), you might consider editing [the default CSP policy](#http_security) to allow your tiles to be displayed.
    * `:attribution` A string to give proper map credits attribution
* `:routing`
    * `:type` one of `:openstreetmap` or `google_maps`


## Anonymous options

### :anonymous

#### :participation

* `:allowed` if anonymous participation is allowed
* `:validation` a map of validation methods. See [validation methods](#validation-methods).

<!-- #### :event_creation

* `:allowed` if anonymous event creation is allowed
* `:validation` a map of validation methods. See [validation methods](#validation-methods). -->

#### :reports

* `:allowed` if anonymous reporting is allowed


#### Validation methods

* `:email`
    * `:enabled` if validation through email is enabled
    * `:confirmation_required` if email confirmation is required (clicking on a link in a sent email)

* `:captcha`
    * `:enabled` if validation through captcha is enabled

## Background jobs

### Oban

See the configuration reference for [Oban](https://github.com/sorentwo/oban#usage).

#### Queues

Mobilizon has the following queues:
 * `search` inserts the events into the search database
 * `mailers` sends the emails
 * `background` perform background tasks
 * `default` everything else

#### Periodic jobs

* `Mobilizon.Service.Workers.BuildSiteMap` builds a `sitemap.xml` file
* `Mobilizon.Service.Workers.RefreshGroups` refreshes remote groups to make sure we didn't miss any content
* `Mobilizon.Service.Workers.CleanOrphanMediaWorker` clean orphan media
* `Mobilizon.Service.Workers.CleanUnconfirmedUsersWorker` clean unconfirmed users

## Resources

### :rich_media

* `:parsers` A list of [metadata parsers](#metadata-parsers)

### Metadata parsers

* `Mobilizon.Service.RichMedia.Parsers.OEmbed`
* `Mobilizon.Service.RichMedia.Parsers.OGP`
* `Mobilizon.Service.RichMedia.Parsers.TwitterCard`
* `Mobilizon.Service.RichMedia.Parsers.Fallback` (just `<title>` and `<meta name="description">`)

### Mobilizon.Service.ResourceProviders

See [Resource Providers](./resource_providers.md).


### :external_resource_providers

See [Resource Providers](./resource_providers.md).

