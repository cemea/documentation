# Partager, signaler, éditer un événement

Chaque événement possède un bouton **Actions**. Les options dépendent de vos permissions sur cet événement.

## Partage

!!! note
    Cette action est disponible pour tout le monde.

Pour partager un événement vous devez cliquer sur le bouton **Actions <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M16,12A2,2 0 0,1 18,10A2,2 0 0,1 20,12A2,2 0 0,1 18,14A2,2 0 0,1 16,12M10,12A2,2 0 0,1 12,10A2,2 0 0,1 14,12A2,2 0 0,1 12,14A2,2 0 0,1 10,12M4,12A2,2 0 0,1 6,10A2,2 0 0,1 8,12A2,2 0 0,1 6,14A2,2 0 0,1 4,12Z" />
</svg>**&nbsp;:

  * **Partager l'événement** <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M21,12L14,5V9C7,10 4,15 3,20C5.5,16.5 9,14.9 14,14.9V19L21,12Z" /> pour partager l'événement via les réseaux sociaux ou par mail&nbsp;:
    ![image de la modale de partage](../../images/event-share-FR.png)
  * **Ajouter à mon agenda** <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M19 19V8H5V19H19M16 1H18V3H19C20.11 3 21 3.9 21 5V19C21 20.11 20.11 21 19 21H5C3.89 21 3 20.1 3 19V5C3 3.89 3.89 3 5 3H6V1H8V3H16V1M11 9.5H13V12.5H16V14.5H13V17.5H11V14.5H8V12.5H11V9.5Z" />
    </svg> vous permet d'ajouter le fichier `.ics` à votre logiciel de calendrier (Thunderbird, par exemple)&nbsp;:
      ![modale de partage ics](../../images/event-share-ics-EN.png)

## Signalement

!!! note
    Vous devez être connecté⋅e à votre compte Mobilizon pour faire une signalement.

Pour signaler un événement, vous devez cliquer sur **Signalement** <svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="currentColor" d="M14.4,6L14,4H5V21H7V14H12.6L13,16H20V6H14.4Z" /></svg>

![modale de signalement d'un événement](../../images/event-report-modal-FR.png)

## Modifier

!!! note
    Vous devez être admin ou modérateur/modératrice pour voir ces options

Pour modifier, dupliquer ou supprimer un événement vous devez cliquer sur le bouton **Actions** puis&nbsp;:

  * **Modifier** (voir [Créer un événement](creation-evenement.md))
  * **Dupliquer** pour ouvrir une page remplie des mêmes paramètres que l'événement
  * **Supprimer** pour supprimer l'événement
