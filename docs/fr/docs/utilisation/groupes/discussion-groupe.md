# Discussions dans un groupe

 Rassemblez sur une seule page l'ensemble de la conversation sur un sujet spécifique.

!!! note
    Vous devez être membre, modérateur ou administrateur du groupe pour pouvoir commencer, modifier et supprimer une discussion.

## Commencer une nouvelle discussion

Pour commencer une nouvelle discussion dans un groupe, vous devez, sur la page du groupe&nbsp;:

  1. cliquer sur le bouton **+ Lancer une discussion** dans la section **Discussions**
  * ajouter un titre
  * ajouter un texte
  * cliquer sur le bouton **Créer la discussion**

!!! note
    Vous pouvez aussi commencer une discussion en cliquant sur **Voir tous** > **Nouvelle discussion**
    ![image voir tous](../../images/group-discussions-view-all-FR.png)

Votre sujet est désormais visible de tout membre du groupe dans la section **Discussions**. Tous les membres peuvent y répondre.

## Modifier et supprimer une discussion

### Modifier une discussion

Pour modifier le titre d'une discussion vous devez&nbsp;:

  1. ouvrir la discussion en cliquant sur le titre dans la liste du groupe
  * cliquer sur <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />
    </svg> à la suite du titre
  * faire vos changements
  * cliquer sur <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M21,7L9,19L3.5,13.5L4.91,12.09L9,16.17L19.59,5.59L21,7Z" />
    </svg> sous le titre pour sauvegarder le changement (ou **x** pour annuler)

![gif discussion title edition](../../images/group-discussion-edit-FR.gif)

### Supprimer une discussion

Pour supprimer une discussion, vous devez&nbsp;:

  1. ouvrir la discussion en cliquant sur le titre dans la liste du groupe
  * cliquer sur <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />
    </svg> en face du titre
  * cliquer sur <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" />
    </svg> **Supprimer la conversation** (ou **x** pour annuler)

!!! note
    Seuls le créateur ou la créatrice de la discussion et les modérateurs et administratrices peuvent supprimer la discussion.

## Modifier et supprimer un commentaire

### Modifier un commentaire

Pour modifier un commentaire vous devez&nbsp;:

  1. cliquer sur **…** à côté de la date du commentaire
  * cliquer sur <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />
    </svg> **Modifier**
  * faire vos changements
  * cliquer sur le bouton **Editer**

### Supprimer un commentaire

  1. cliquer sur **…** à côté de la date du commentaire
  * cliquer sur <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" />
    </svg> **Supprimer**
