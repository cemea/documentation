# Comment créer un compte

Pour pouvoir créer différents profils, événements et groupes, vous devez vous créer un compte.

## Inscription

Une fois que vous avez trouvé une instance qui vous convient vous devez cliquer sur le bouton **S'inscrire** en haut, à droite de la page. Ensuite, vous devez&nbsp;:

=== "Comment faire"

    1. entrer une adresse mail
    * entrer un mot de passe
    * cocher **J'accepte les règles de l'instance et les conditions générales d'utilisation** (**après** les avoir lues, bien sûr ;))
    * cliquer le bouton **Créer un compte**

=== "Capture d'écran"

    ![image de la page d'inscription](../../images/rose-register-FR.png)

!!! note
    A partir de cet instant vous devriez avoir reçu un mail avec un lien d'activation pour votre compte. Dans le cas contraire, cliquez sur le lien **Vous n'avez pas reçu les instructions ?**

## Activation

=== "Comment faire"
    Une fois votre compte activé vous devez&nbsp;:

      1. indiquer un **pseudonyme affiché** (requis) : ce que verront les personnes
      * indiquer un **identifiant** (requis) : cet identifiant est unique à votre profil. Il permet à d'autres personnes de vous trouver.
      * remplir une **courte biographie** (facultatif) : pour en dire plus vous concernant :)

    Une fois les champs remplis vous pouvez cliquer sur le bouton **Créer mon profil**.

=== "Capture d'écran"

    ![image création de compte](../../images/rose-utopia-create-account-filled-FR.png)

## Paramètres

=== "Comment faire"
    **Fuseau horaire**

     Mobilizon utilise votre fuseau horaire pour s'assurer que vous recevez les notifications pour un événement au bon moment. Si le fuseau horaire détecté est erroné vous pouvez le corriger en cliquant sur le bouton **Gérer mes paramètres**.

    **Notifications de participation**

      Mobilizon vous enverra un email quand les événements auxquels vous participez connaissent des événements importants : date et heure, adresse, confirmation ou annulation, etc. Si vous ne souhaitez pas les recevoir vous pouvez décocher la case **Notification le jour de l'événement**.

    Quand tout est correct, cliquez sur le bouton **C'est tout bon, continuons !**

=== "Capture d'écran"

    ![image des paramètres](../../images/creation-account-settings-FR.png)
