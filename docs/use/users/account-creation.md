# Create an account

To be able to create identities, events and groups, you have to create an account.

## Sign up

Once you've found an instance that suits you, click the **Sign up** button on the top bar. Then:

=== "How to"

    1. enter your email address
    * enter a password
    * check the **I agree the instance rules and terms of service** box (**after** reading them of course ;) )
    * click the **Register** button.

=== "Screenshot"
    ![sign up page](../../images/en/rose-register-EN.png)

!!! note
    At this point you will receive an email with a link to activate your account. If you don't, click the **Didn't receive the instructions?** link.

## Activation

=== "How to"
    Once activation is done, you have to fill out:

      1. a **Display name** (required): what other people will see
      * a **Username** (required): your unique identifier for your account on this and all the other instances. It's as unique as an email address, which makes it easy for other people to interact with it.
      * a **Bio**: tell people more about you.

    When everything is done, click the **Create my profile** button.

=== "Screenshot"
    ![bio page settings](../../images/en/rose-utopia-create-account-filled-EN.png)

## Settings

=== "How to"
    **Time Zone**

    Mobilizon uses your time zone to make sure you get notifications for an event at the correct time. If the detected time zone isn't correct, you can change it by clicking the **Manage my settings** button.

    **Participation notifications**

    Mobilizon will send you an email when the events you are attending have important changes: date and time, address, confirmation or cancellation, etc. If you don't want those notifications: uncheck the **Notification on the day of the event** box.

    When everything is ok for you, click the **All good, let's continue!** button.

=== "Screenshot"
    ![settings image](../../images/en/creation-account-settings-EN.png)
