# Create an event

Let's create a new event!

To do so, click the **Create** button on the top bar. Then, you need to fill in some information about your event to help gather people together!

!!! note
    At any time you can save a draft of your event by clicking the **Save draft** button on the fixed bottom bar.
    ![image: save panel](../../images/en/create-event-save.png)

## General information

=== "Empty"
    ![image: create event empty](../../images/en/create-event-general-information-empty.png)

=== "Filled"
    ![image: create event filled](../../images/en/create-event-general-information-filled.png)

In this section you can:

  * add a **Headline picture** to have a banner on your event page by clicking **Click to upload** and choosing a picture that suits you
  * add a **Title** (required)
  * add some **tags** (10 max) to easily distinguish your event from others that are similar (tags are added by pressing `Enter` key or by seperating them by commas)
  * set a start and end date to your event. You also can show/hide the time when the event begins/ends by clicking **Date parameters**
  * add an address: you can either enter the address or locate your address by clicking the <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M12,11.5A2.5,2.5 0 0,1 9.5,9A2.5,2.5 0 0,1 12,6.5A2.5,2.5 0 0,1 14.5,9A2.5,2.5 0 0,1 12,11.5M12,2A7,7 0 0,0 5,9C5,14.25 12,22 12,22C12,22 19,14.25 19,9A7,7 0 0,0 12,2Z" /></svg> icon. Once done, location will be displayed on an OpenStreetMap map.
  * add a **description**
  * add a Website or any URL

## Organizers

This section allows you to pick a profile or group as an organizer. If you choose a group, you can pick one or more contact(s) among the group.

## Who can view this event and participate

=== "No limit"
    ![image: create event who participe](../../images/en/create-event-who.png)

=== "Limited"
    ![image: create event who participe](../../images/en/create-event-who-limited.png)

This is where you can decide who can view and particpate in your event!

  * **Visible everywhere on the web (public)**: anybody can see it by browsing your mobilizon instance
  * **Only accessible through link (private)**: only people who have the link can see your event

**Anonymous participation**: if you want to allow people to participate without an account.

!!! note
    Anonymous participants will be asked to confirm their participation through e-mail.

**Participation approval**: if you want to approve every parcipation request (participation notifications can be set [in your profile](../../users/account-settings/#participation-notifications))

**Number of places**: if you want to limit this number

## Public comment moderation

![image: comment moderation](../../images/en/create-event-comment-moderation.png)

In this section you can set whether you allow comments or not.

## Status

![image: status of your event](../../images/en/create-event-status.png)

Here you can set if your event is tentative and will be confirmed later, will happen, or has been cancelled.
