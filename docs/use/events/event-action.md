# Share, report, edit an Event

Every event has an **Actions** button. Options depend on your permissions.

## Share

!!! note
    This action is available for everyone

To share or add an event to your calendar, click the **Actions <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M16,12A2,2 0 0,1 18,10A2,2 0 0,1 20,12A2,2 0 0,1 18,14A2,2 0 0,1 16,12M10,12A2,2 0 0,1 12,10A2,2 0 0,1 14,12A2,2 0 0,1 12,14A2,2 0 0,1 10,12M4,12A2,2 0 0,1 6,10A2,2 0 0,1 8,12A2,2 0 0,1 6,14A2,2 0 0,1 4,12Z" />
</svg>** button.

  * **Share this event** <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M21,12L14,5V9C7,10 4,15 3,20C5.5,16.5 9,14.9 14,14.9V19L21,12Z" />
    </svg> to share it on your social media network or email:
    ![share modal image](../../images/en/event-share-EN.png)
  * **Add to my calendar <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M19 19V8H5V19H19M16 1H18V3H19C20.11 3 21 3.9 21 5V19C21 20.11 20.11 21 19 21H5C3.89 21 3 20.1 3 19V5C3 3.89 3.89 3 5 3H6V1H8V3H16V1M11 9.5H13V12.5H16V14.5H13V17.5H11V14.5H8V12.5H11V9.5Z" />
    </svg>** lets you add the `.ics` file in to your calendar software (Thunderbird for example):

    ![share ics modal image](../../images/en/event-share-ics-EN.png)

## Report

!!! note
    You must be connected to your mobilizon account to see this action

In addition to the actions listed above, you also can report an event by clicking the **Report <svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="currentColor" d="M14.4,6L14,4H5V21H7V14H12.6L13,16H20V6H14.4Z" /></svg>** button.

![image event report modal](../../images/en/event-report-modal-EN.png)

## Edit

!!! note
    You must have correct the permission to see these actions (admin/moderator)

To **Edit**, **Duplicate** or **Delete** your event, click the **Actions <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M16,12A2,2 0 0,1 18,10A2,2 0 0,1 20,12A2,2 0 0,1 18,14A2,2 0 0,1 16,12M10,12A2,2 0 0,1 12,10A2,2 0 0,1 14,12A2,2 0 0,1 12,14A2,2 0 0,1 10,12M4,12A2,2 0 0,1 6,10A2,2 0 0,1 8,12A2,2 0 0,1 6,14A2,2 0 0,1 4,12Z" />
</svg>** button.

  * **Edit**: see [Create an event](create-events.md)
  * **Duplicate** will open a new event pre-filled with the settings from the current event
  * **Delete** will delete your event
