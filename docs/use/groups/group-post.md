# Group post

A place to publish something to the whole world, your community, or just your group members.

## Create group post

To create a new post, you go to your group managing page and click the **+ Post a public message** button:

![image public page post](../../images/en/group-public-post.png)

At this point you can:

  1. add a banner
  * add a title (required)
  * add some tags
  * add your post text (required)
  * set who can see it:
    * (default) Visible everywhere on the web
    * Only accessible through link
    * Only accessible to members of the group

Next: click the **Publish** button to publish or **Save draft** if you want to publish later.

![image empty add new post template](../../images/en/group-add-new-post-template-empty.png)

## Edit a group post

To edit a post you have to be connected to your account and, on the post page, click on the **Edit** link below the post date.

![image edit post link](../../images/en/group-post-edit-link.png)

Once done, click the **Update post** button.

## Delete a group post

To delete a post you have to be connected to your account and, on the post page, click on the **Edit** link below the post date (see above) and click the **Delete post** button.
