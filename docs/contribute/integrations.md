# Integrations

Mobilizon provides [a GraphQL API](./graphql_api.md) and can be integrated with other platforms or tools.

## Libraries

Nothing here yet.

## Tools

* [mobilizon-poster](https://github.com/vecna/mobilizon-poster)  
  A simple nodejs script(s) to interact with mobilizon via command line

## CMS

* [Wordpress plugin](https://wordpress.org/plugins/connector-mobilizon/) ([Source on Github](https://github.com/dwaxweiler/connector-mobilizon))
* [Jekyll plugin](https://github.com/Marc-AntoineA/jekyll-mobilizon)
